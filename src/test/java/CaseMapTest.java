import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CaseMapTest {
    MapReduceDriver<Object, Text, Text, CMOutput, Text, CMOutput> mapReduceDriver;
    MapDriver<Object, Text, Text, CMOutput> mapDriver;
    ReduceDriver<Text, CMOutput, Text, CMOutput> reduceDriver;


    @Before
    public void setUp() {
        CaseMap.CMMapper mapper = new CaseMap.CMMapper();
        CaseMap.CMReducer reducer = new CaseMap.CMReducer();
        mapDriver = new MapDriver<Object, Text, Text, CMOutput>();
        mapDriver.setMapper(mapper);
        reduceDriver = new ReduceDriver<Text, CMOutput, Text, CMOutput>();
        reduceDriver.setReducer(reducer);
        mapReduceDriver = new MapReduceDriver<Object, Text, Text, CMOutput, Text, CMOutput>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
    }

    @Test
    public void testMapper() {
        System.out.println("\nMapper Testing");
        mapDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:06:56:30 ]" +
                " \"GET /seatposts HTTP/1.0\" 101 3730 \"http://www.casualcyclist.com\" \"Mozilla/5.0" +
                " (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\""));
        mapDriver.withOutput(new Text("81.73.150.239"), new CMOutput(1.0f, 101.0f));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() {
        System.out.println("\nReducer Testing");
        List<CMOutput> values = new ArrayList<CMOutput>();
        values.add(new CMOutput(1.0f, 101.0f));
        values.add(new CMOutput(1.0f, 105.0f));
        reduceDriver.withInput(new Text("81.73.150.239"), values);
        reduceDriver.withOutput(new Text("81.73.150.239"), new CMOutput(103.0f, 206.0f));
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() {
        System.out.println("\nMapReduce Testing");
        mapReduceDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:06:56:30 ]" +
                " \"GET /seatposts HTTP/1.0\" 101 3730 \"http://www.casualcyclist.com\" \"Mozilla/5.0" +
                " (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\"\n" +
                "10.182.189.79 - - [10/Oct/2013:06:59:12 ] \"GET /saddles HTTP/1.0\" 102 2609 \"-\"" +
                " \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko)" +
                " Chrome/36.0.1944.0 Safari/537.36\"\n" +
                "16.180.70.237 - - [10/Oct/2013:07:00:38 ] \"GET /wheelsets HTTP/1.0\" 103 3246" +
                " \"http://bleater.com\" \"Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\"\n" +
                "81.73.150.239 - - [10/Oct/2013:07:05:42 ] \"GET /seatposts HTTP/1.0\" 105 3416" +
                " \"http://bestcyclingreviews.com/top_online_shops\" \"Mozilla/5.0 (Windows NT 5.1; rv:31.0)" +
                " Gecko/20100101 Firefox/31.0\""));
        mapReduceDriver.addOutput(new Text("10.182.189.79"), new CMOutput(102.0f, 102.0f));
        mapReduceDriver.addOutput(new Text("16.180.70.237"), new CMOutput(103.0f, 103.0f));
        mapReduceDriver.addOutput(new Text("81.73.150.239"), new CMOutput(103.0f, 206.0f));

        mapReduceDriver.runTest();
    }
}