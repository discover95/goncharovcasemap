import java.io.IOException;
import java.util.regex.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import static org.apache.hadoop.mapreduce.lib.output.FileOutputFormat.setOutputPath;

/**
 * @author Goncharov Dmitry
 * CaseMap class
 */
public class CaseMap {
    /**
     * Mapper class
     */
    public static class CMMapper
            extends Mapper<Object, Text, Text, CMOutput>{

        private final static CMOutput val = new CMOutput(1, 1);
        private Text word = new Text();

        String pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +              // ip
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5]))" +
                "\\s-\\s-\\s" +                                                 //   - -
                "\\[\\d\\d/[a-zA-Z]+/\\d{4}(:\\d\\d){3}\\s(-[0-9]{4})?\\]" +    //  [dd/mm/yyyy:hh:mm:ss ]
                "\\s\"GET\\s[a-zA-Z0-9/\\.-]+\\s[a-zA-Z0-9/\\.-]+\"" +          //
                "\\s([0-9]+)\\s([0-9]+)";                           //

        Pattern p = Pattern.compile(pattern);

        /**
         * Process row (web-log) and emit Ip:{bytes_per_request, total_bytes}
         * @param key - unused Object
         * @param value - Text value
         * @param context  - context
         * @throws IOException
         * @throws InterruptedException
         */
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            Matcher m = p.matcher(value.toString());
            while (m.find()) {
                word.set(m.group(1));
                val.set(1, Integer.parseInt(m.group(8)));
                context.write(word, val);
            }
        }
    }

    /**
     * Reducer class
     */
    public static class CMReducer
            extends Reducer<Text, CMOutput, Text, CMOutput> {
        private CMOutput result = new CMOutput();

        /**
         * Emit Ip:{bytes_per_request, total_bytes}
         * @param key
         * @param values
         * @param context
         * @throws IOException
         * @throws InterruptedException
         */
        public void reduce(Text key, Iterable<CMOutput> values,
                           Context context
        ) throws IOException, InterruptedException {
            float number = 0, all_value = 0;
            for (CMOutput val : values) {
                number += val.getLeft().get();
                all_value += val.getRight().get();
            }

            result.set(all_value / number, all_value);
            context.write(key, result);
        }
    }

    /**
     * Main method
     * @param args : args[0] - input folder, args[1] - output folder
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        /**
         * Set separator ";"
         */
        conf.set("mapred.textoutputformat.separator", ";");
        Job job = Job.getInstance(conf, "case map");
        job.setJarByClass(CaseMap.class);

        /**
         * Mapper and Reducer config
         */
        job.setMapperClass(CMMapper.class);
        job.setReducerClass(CMReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(CMOutput.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        setOutputPath(job, new Path(args[1]));

        /**
         * Set .csv output
         */
        boolean success = job.waitForCompletion(true);
        if (success){
            FileSystem hdfs = FileSystem.get(conf);
            FileStatus fs[] = hdfs.listStatus(new Path(args[1]));
            if (fs != null){
                for (FileStatus aFile : fs) {
                    if (!aFile.isDir()) {
                        hdfs.rename(aFile.getPath(), new Path(aFile.getPath().toString()+".csv"));
                    }
                }
            }
        }
        System.exit(success ? 0 : 1);
    }
}
