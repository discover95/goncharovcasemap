import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.WritableComparable;

/**
 * @author Goncharov Dmitry
 * Class value for mapper and reducer
 */
public class CMOutput implements WritableComparable<CMOutput> {
    private FloatWritable left_value;
    private FloatWritable right_value;

    public CMOutput() {
        set(new FloatWritable(), new FloatWritable());
    }

    public CMOutput(FloatWritable left_value, FloatWritable right_value) {
        this.set(left_value, right_value);
    }

    public CMOutput(float left_value, float right_value) {
        this.set(left_value, right_value);
    }

    public void set(FloatWritable left_value, FloatWritable right_value) {
        this.left_value = left_value;
        this.right_value = right_value;
    }

    public void set(float left_value, float right_value) {
        this.left_value = new FloatWritable(left_value);
        this.right_value = new FloatWritable(right_value);
    }

    public FloatWritable getLeft()
    {
        return left_value;
    }

    public FloatWritable getRight()
    {
        return right_value;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        left_value.readFields(in);
        right_value.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        left_value.write(out);
        right_value.write(out);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CMOutput)) {
            return false;
        } else {
            CMOutput other = (CMOutput)o;
            return (left_value.equals(other.left_value) && right_value.equals(other.right_value));
        }
    }

    @Override
    public int hashCode() {
        return left_value.hashCode()*163 + right_value.hashCode();
    }

    @Override
    public int compareTo(CMOutput cmou) {
        int cmp = left_value.compareTo(cmou.left_value);
        if (cmp != 0) {
            return cmp;
        }
        return right_value.compareTo(cmou.right_value);
    }

    @Override
    public String toString() {
        return left_value.toString() + ";" + right_value.toString();
    }
}
